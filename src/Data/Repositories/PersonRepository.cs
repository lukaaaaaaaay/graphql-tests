﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphQLTests.Data;
using GraphQLTests.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories
{
    public class PersonRepository
    {
        private readonly GraphQlContext _context;

        public PersonRepository(GraphQlContext context)
        {
            _context = context;
        }

        public List<Person> GetPeople()
        {
            return _context.People.Include(x => x.Vehicles).Include(x => x.Properties).ToList();
        }

        public async Task<List<Person>> GetPeopleAsync()
        {
            return await _context.People.Include(x => x.Vehicles).Include(x => x.Properties).ToListAsync();
        }

        public async Task<Person> GetPersonAsync(Guid id)
        {
            return await _context.People.Include(x => x.Vehicles).Include(x => x.Properties).FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
