﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphQLTests.Data.Models;

namespace GraphQLTests.Data
{
    public static class DbInitialiser
    {
        /// <summary>
        /// Adds in some dummy data if the database is empty.
        /// </summary>
        /// <param name="context"></param>
        public static void Init(GraphQlContext context)
        {
            context.Database.EnsureCreated();

            if (context.People.Any()) 
                return;

            var vehicles = new Vehicle[]
            {
                new Vehicle
                {
                    VehicleType = VehicleType.Car,
                    Make = "Toyota",
                    Model = "Tacoma",
                    RegistrationNumber = "DFD7123",
                    YearOfManufacture = "2018"
                },
                new Vehicle
                {
                    VehicleType = VehicleType.Car,
                    Make = "Toyota",
                    Model = "Four Runner",
                    RegistrationNumber = "JD87H66",
                    YearOfManufacture = "2019"
                },
                new Vehicle
                {
                    VehicleType = VehicleType.Car,
                    Make = "Chevrolet",
                    Model = "Malibu",
                    RegistrationNumber = "UIL9D38",
                    YearOfManufacture = "2018"
                },
                new Vehicle
                {
                    VehicleType = VehicleType.Car,
                    Make = "Toyota",
                    Model = "Prius",
                    RegistrationNumber = "W67DA7",
                    YearOfManufacture = "2018"
                }
            };

            foreach (var vehicle in vehicles)
                context.Vehicles.Add(vehicle);

            context.SaveChanges();

            var properties = new Property[]
            {
                new Property
                {
                    Address = "Hermosa Avenue",
                    City = "Hermosa Beach",
                    Country = "USA",
                    Postcode = "90254",
                    PropertyNumber = "206",
                    PropertyType = PropertyType.Apartment,
                    State = "CA",
                    StreetNumber = "206"
                },
                new Property
                {
                    Address = "Dingley Close",
                    City = "Gladstone Park",
                    Country = "Australia",
                    Postcode = "3043",
                    PropertyType = PropertyType.House,
                    State = "VIC",
                    StreetNumber = "8"
                },
                new Property
                {
                    Address = "Blackburn Road",
                    City = "Blackburn",
                    Country = "Australia",
                    Postcode = "3130",
                    PropertyType = PropertyType.House,
                    State = "VIC",
                    StreetNumber = "49"
                },
                new Property
                {
                    Address = "Waverley Road",
                    City = "Malvern East",
                    Country = "Australia",
                    Postcode = "3145",
                    PropertyType = PropertyType.TownHouse,
                    State = "VIC",
                    StreetNumber = "602-606",
                    PropertyNumber = "9"            
                }
            };

            foreach (var property in properties)
                context.Properties.Add(property);

            context.SaveChanges();

            var people = new Person[]
            {
                new Person
                {
                    FirstName = "Luke",
                    LastName = "Phelan",
                    EmailAddress = "laphelan@gmail.com",
                    PhoneNumber = "+14243501530",
                    Properties = new List<Property>
                    {
                        properties[2], properties[3]
                    },
                    Vehicles = new List<Vehicle> { vehicles[2] }
                },
                new Person
                {
                    FirstName = "Kate",
                    LastName = "Phelan",
                    EmailAddress = "katephelan@live.com",
                    PhoneNumber = "+17573866655",
                    Properties = new List<Property>
                    {
                        properties[1]
                    },
                    Vehicles = new List<Vehicle> { vehicles[3] }
                },
                new Person
                {
                    FirstName = "Matthew",
                    LastName = "Hetherington",
                    PhoneNumber = "+17576671760",
                    Properties = new List<Property>
                    {
                        properties[0]
                    },
                    Vehicles = new List<Vehicle> { vehicles[0], vehicles[1] }
                },
            };

            foreach (var person in people)
                context.People.Add(person);

            context.SaveChanges();
        }
    }
}
