﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GraphQLTests.Data.Models
{
    public class Person
    {
        [Key]
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        [EmailAddress]
        public string EmailAddress { get; set; }

        public string PhoneNumber { get; set; }

        public virtual ICollection<Property> Properties { get; set; }

        public virtual ICollection<Vehicle> Vehicles { get; set; }

        [NotMapped]
        public string FullName => $"{FirstName} {LastName}";

    }
}
