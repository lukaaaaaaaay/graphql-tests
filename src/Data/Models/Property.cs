﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphQLTests.Data.Models
{
    public class Property
    {
        [Key]
        public Guid Id { get; set; }

        public PropertyType PropertyType { get; set; }

        // use this for things like apartment or unit numbers
        public string PropertyNumber { get; set; }

        public string StreetNumber { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Postcode { get; set; }

        public string Country { get; set; }
        

    }

    public enum PropertyType
    {
        House,
        Apartment,
        Unit,
        TownHouse
    }
}
