﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphQLTests.Data.Models
{
    public class Vehicle
    {
        [Key]
        public Guid Id { get; set; }

        public VehicleType VehicleType { get; set; }

        public string RegistrationNumber { get; set; }

        public string Make { get; set; }

        public string Model { get; set; }

        public string YearOfManufacture { get; set; }
    }

    public enum VehicleType
    {
        Car,
        Motorbike,
        Boat,
        Truck
    }
}
