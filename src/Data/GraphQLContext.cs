﻿using GraphQLTests.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace GraphQLTests.Data
{
    public class GraphQlContext : DbContext
    {
        public GraphQlContext(DbContextOptions<GraphQlContext> options) : base(options)
        {
        }

        public DbSet<Person> People { get; set; }
        public DbSet<Property> Properties { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }

    }
}
