﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Repositories;
using GraphQL.Builders;
using GraphQL.Execution;
using GraphQL.Types;

namespace GraphQLTests.Web.Models
{
    public class PeopleQuery : ObjectGraphType
    {
        public PeopleQuery(PersonRepository personRepository)
        {
            FieldAsync<ListGraphType<PersonType>>("people", resolve: async context => await personRepository.GetPeopleAsync());
            FieldAsync<PersonType>("person",
                arguments: new QueryArguments(new QueryArgument<StringGraphType>() {Name = "id"}),
                resolve: async context =>
                {
                    var id = context.GetArgument<string>("id");
                    return await personRepository.GetPersonAsync(Guid.Parse(id));
                });
        }
    }
}