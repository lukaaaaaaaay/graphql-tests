﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.Types;
using GraphQLTests.Data.Models;

namespace GraphQLTests.Web.Models
{
    public class PropertyType : ObjectGraphType<Property>
    {
        public PropertyType()
        {
            Name = "Property";
            Description = "A dwelling that a person owns";
            Field(p => p.PropertyNumber, nullable: true)
                .Description("The property number of the property. It could be an apartment or unit number.");
            Field(p => p.StreetNumber).Description("The street number where the property is located.");
            Field(p => p.Address).Description("The name of the street the property is located.");
            Field(p => p.City).Description("The city/suburb the property is located.");
            Field(p => p.State).Description("The state the property is located.");
            Field(p => p.Country).Description("The country the property is located.");
            Field(p => p.Postcode).Description("The postcode of the property.");
            Field<PropertyTypeEnum>("propertyType", "The type of property");
        }

        public class PropertyTypeEnum : EnumerationGraphType<GraphQLTests.Data.Models.PropertyType>
        {
        }
    }
}
