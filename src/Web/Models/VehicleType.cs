﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.Types;
using GraphQLTests.Data.Models;

namespace GraphQLTests.Web.Models
{
    public class VehicleType : ObjectGraphType<Vehicle>
    {
        public VehicleType()
        {
            Name = "Vehicle";
            Description = "A motorised form of transport that People can own";
            Field(v => v.Make).Description("The make of the vehicle");
            Field(v => v.Model).Description("The model of the vehicle");
            Field(v => v.RegistrationNumber).Description("The vehicles registration number");
            Field(v => v.YearOfManufacture).Description("The year the vehicle was manufactured");
            Field<VehicleTypeEnum>("vehicleType", "The type of the vehicle");
        }

    }

    public class VehicleTypeEnum : EnumerationGraphType<GraphQLTests.Data.Models.VehicleType>
    {
    }
}
