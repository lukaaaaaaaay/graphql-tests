﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.Types;
using GraphQLTests.Data.Models;

namespace GraphQLTests.Web.Models
{
    public class PersonType : ObjectGraphType<Person>
    {
        public PersonType()
        {
            Name = "Person";
            Description = "A human that can own either vehicles or properties";
            //Field("id", resolve: x => x.).Description("The id of the person in the database.");
            Field<StringGraphType>("id", resolve: context => context.Source.Id.ToString());
            Field(p => p.FullName, nullable: true).Description("The full name of the person.");
            Field(p => p.EmailAddress, nullable: true).Description("The email of the person");
            Field(p => p.PhoneNumber, nullable: true).Description("The phone number of the person");
            Field<ListGraphType<VehicleType>>("vehicles", "The different vehicles the person owns");
            Field<ListGraphType<PropertyType>>("properties", "The different properties the person owns.");
        }
    }
}
