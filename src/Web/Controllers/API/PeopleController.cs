﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Routing;
using Data.Repositories;
using GraphQL;
using GraphQL.Types;
using GraphQLTests.Data;
using GraphQLTests.Data.Models;
using GraphQLTests.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GraphQLTests.Web.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class PeopleController : ControllerBase
    {
        private readonly PersonRepository _personRepository;

        public PeopleController(PersonRepository personRepository)
        {
            _personRepository = personRepository;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Person>), 200)]
        public async Task<IActionResult> Get()
        {
            var query = @"
                    query PeopleQuery {
                        people {
                            id
                            fullName
                            emailAddress
                            phoneNumber
                            vehicles {
                                vehicleType
                                make
                                model
                                registrationNumber
                                yearOfManufacture
                            }
                            properties {
                                propertyNumber
                                streetNumber
                                address
                                city
                                state
                                country
                                postcode
                                propertyType
                            }
                        }
                    }
                ";
            var id = "3A0D45DE-0902-43D8-DCC5-08D68731AF8C";
            var singleQuery = $@" person(id: ""{id}"") {{
                            fullName
                            emailAddress
                            phoneNumber
                            vehicles {{
                                vehicleType
                                make
                                model
                                registrationNumber
                                yearOfManufacture
                            }}
                            properties {{
                                propertyNumber
                                streetNumber
                                address
                                city
                                state
                                country
                                postcode
                                propertyType
                            }}
                        }}
                ";

            var schema = new Schema {Query = new PeopleQuery(_personRepository)};
            var json = await schema.ExecuteAsync(q =>
            {
                q.OperationName = "PeopleQuery";
                q.Query = query;
            });
            return Ok(json);
        }

        // GET: api/people/{id} 
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(Person), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(string id)
        {
            var person = await _personRepository.GetPersonAsync(Guid.Parse(id));
            if (person == null)
                return NotFound();

            return Ok(person);
        }
    }
}